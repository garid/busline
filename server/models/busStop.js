import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const busStopSchema = new Schema({
  dateAdded	: { type: 'Date', default: Date.now, required: true },
  lat: { type: 'String', required: true },
  lon: { type: 'String', required: true },
  stopId: { type: 'Number', required: true },
  cuid: { type: 'String', required: true },
});

export default mongoose.model('BusStop', busStopSchema);
