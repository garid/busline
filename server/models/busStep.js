import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const busStepSchema = new Schema({
  dateAdded: { type: 'Date', default: Date.now, required: true },
  stopId: { type: 'Number', required: true },
  nextId: { type: 'Number', required: true },
  lineId: { type: 'Number', required: true },
  cuid: { type: 'String', required: true },
});

export default mongoose.model('BusStep', busStepSchema);
