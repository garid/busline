import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const busLineSchema = new Schema({
  dateAdded: { type: 'Date', default: Date.now, required: true },
  lineId: { type: 'String', unique: true },
  code: { type: 'String', required: true},
  title: { type: 'String', required: true},
  cuid: { type: 'String', required: true },
});
export default mongoose.model('BusLine', busLineSchema);