import { Router } from 'express';
import * as BusLineController from '../controllers/busLine.controller';
const router = new Router();

router.route('').get(BusLineController.getBusLines);
router.route('/:lineId').get(BusLineController.getBusLine);
router.route('').post(BusLineController.addBusLine);
router.route('/:lineId').delete(BusLineController.deleteBusLine);

export default router;
