import { Router } from 'express';
import * as BusStepController from '../controllers/busStep.controller';
const router = new Router();

router.route('').get(BusStepController.getBusSteps);
router.route('/:lineId,:stopId').get(BusStepController.getBusStep);
router.route('').post(BusStepController.addBusStep);
router.route('/:lineId&:stopId').delete(BusStepController.deleteBusStep);

export default router;
