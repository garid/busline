import { Router } from 'express';
import * as BusStopController from '../controllers/busStop.controller';
const router = new Router();

router.route('').get(BusStopController.getBusStops);
router.route('/:stopId').get(BusStopController.getBusStop);
router.route('').post(BusStopController.addBusStop);
router.route('/:stopId').delete(BusStopController.deleteBusStop);

export default router;
