import BusStop from '../models/busStop';
import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';

/**
 * Get all stops
 * @param req
 * @param res
 * @returns void
 */
export function getBusStops(req, res) {
  BusStop.find().sort('-dateAdded').exec((err, busStops) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busStops });
  });
}

/**
 * Save a bus stop
 * @param req
 * @param res
 * @returns void
 */
export function addBusStop(req, res) {
  if (!req.body.busStop.lat || !req.body.busStop.lon || !req.body.busStop.stopId) {
    res.status(403).end();
  }

  const newBusStop = new BusStop(req.body.busStop);

  // Let's sanitize inputs
  newBusStop.stopId = sanitizeHtml(newBusStop.stopId);
  newBusStop.lat = sanitizeHtml(newBusStop.lat);
  newBusStop.lon = sanitizeHtml(newBusStop.lon);

  // newBusStop.slug = slug(newBusStop.stopId.toLowerCase(), { lowercase: true });
  newBusStop.cuid = cuid();
  newBusStop.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busStop: saved });
  });
}

/**
 * Get a single busStop
 * @param req
 * @param res
 * @returns void
 */
export function getBusStop(req, res) {
  BusStop.findOne({ stopId: req.params.stopId }).exec((err, busStop) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busStop });
  });
}

/**
 * Delete a busStop
 * @param req
 * @param res
 * @returns void
 */
export function deleteBusStop(req, res) {
  BusStop.findOne({ stopId: req.params.stopId }).exec((err, busStop) => {
    if (err) {
      res.status(500).send(err);
    }

    busStop.remove(() => {
      res.status(200).end();
    });
  });
}
