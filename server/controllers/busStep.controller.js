import BusStep from '../models/busLine';
import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';

/**
 * Get all stops
 * @param req
 * @param res
 * @returns void
 */
export function getBusSteps(req, res) {
  BusStep.find().sort('-dateAdded').exec((err, busSteps) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busSteps });
  });
}

/**
 * Save a bus stop
 * @param req
 * @param res
 * @returns void
 */
export function addBusStep(req, res) {
  if (!req.body.busStop.stopId || !req.body.busStop.nextId || !req.body.busStop.lineId) {
    res.status(403).end();
  }

  const newBusStep = new BusStop(req.body.busStep);

  // Let's sanitize inputs
  newBusStep.stopId = sanitizeHtml(newBusStep.stopId);
  newBusStep.nextId = sanitizeHtml(newBusStep.nextId);
  newBusStep.lineId = sanitizeHtml(newBusStep.lineId);

  // newBusStep.slug = slug(newBusStep.stopId.toLowerCase(), { lowercase: true });
  newBusStep.cuid = cuid();
  newBusStep.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busStep: saved });
  });
}

/**
 * Get a single busStep
 * @param req
 * @param res
 * @returns void
 */
export function getBusStep(req, res) {
  BusStep.findOne({ stopId: req.params.stopId, lineId: req.params.lineId }).exec((err, busStep) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busStep });
  });
}

/**
 * Delete a busStep
 * @param req
 * @param res
 * @returns void
 */
export function deleteBusStep(req, res) {
  BusStep.findOne({ stopId: req.params.stopId, lineId: req.params.lineId }).exec((err, busStep) => {
    if (err) {
      res.status(500).send(err);
    }

    busStep.remove(() => {
      res.status(200).end();
    });
  });
}
