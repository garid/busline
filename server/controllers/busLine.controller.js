import BusLine from '../models/busLine';
import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';

/**
 * Get all stops
 * @param req
 * @param res
 * @returns void
 */
export function getBusLines(req, res) {
  BusLine.find().sort('-dateAdded').exec((err, busLines) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busLines });
  });
}

/**
 * Save a bus stop
 * @param req
 * @param res
 * @returns void
 */
export function addBusLine(req, res) {
  if (!req.body.busLine.code || !req.body.busLine.title) {
    res.status(403).end();
  }

  const newBusLine = new BusLine(req.body.busLine);

  // Let's sanitize inputs
  // newBusLine.lineId = sanitizeHtml(newBusLine.lineId);
  newBusLine.code = sanitizeHtml(newBusLine.code);
  newBusLine.title = sanitizeHtml(newBusLine.title);

  // newBusLine.slug = slug(newBusLine.stopId.toLowerCase(), { lowercase: true });
  newBusLine.cuid = cuid();
  newBusLine.lineId = cuid();
  newBusLine.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busLine: saved });
  });
}

/**
 * Get a single busLine
 * @param req
 * @param res
 * @returns void
 */
export function getBusLine(req, res) {
  BusLine.findOne({ lineId: req.params.lineId }).exec((err, busLine) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ busLine });
  });
}

/**
 * Delete a busStop
 * @param req
 * @param res
 * @returns void
 */
export function deleteBusLine(req, res) {
  BusLine.findOne({ lineId: req.params.lineId }).exec((err, busLine) => {
    if (err) {
      res.status(500).send(err);
    }

    busLine.remove(() => {
      res.status(200).end();
    });
  });
}
